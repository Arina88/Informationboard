﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Название экзамена
            string name = "Программирование";
            //Дата и время начала экзамена
            DateTime date1 = new DateTime(2021, 9, 15);
            DateTime date2 = new DateTime(2021, 9, 15, 10, 00, 00);
            //Время окончания экзамена
            DateTime date3 = new DateTime(2021, 9, 15, 13, 30, 00);
            // Количество заданий
            int a;
            a = 10;
            // Количество баллов за каждое задание
            int b;
            b = 3;
            //Максимальное количество баллов за весь экзамен
            int s;
            s = a * b;
            //Номер аудитории
            int w;
            w = 325;
            //Количество сдающих
            int t;
            t = 45;
            //Необходимые условия для успешного выполнения экзамена
            //Наличие таких программ,как
            string name1 = "Visual Studio 2022";
            string name2 = "Gitlab";
            string name3 = "любая программа для демонстрации экрана";
            Console.WriteLine($"Экзамен по предмету:{name}");
            Console.WriteLine($"Дата проведения экзамена:{date1.ToLongDateString()}");
            Console.WriteLine($"Время начала:{date2.ToShortTimeString()}");
            Console.WriteLine($"Время окончания:{date3.ToShortTimeString()}");
            Console.WriteLine($"Количество заданий:{a}");
            Console.WriteLine($"Максимальная оценка за тест:{s} баллов");
            Console.WriteLine($"Количество сдающих:{t}человек");
            Console.WriteLine($"Номер аудитории:{w}");
            Console.WriteLine($"Необходимо наличие таких программ,как:{name1},{name2},{name3};");
            Console.ReadKey();
        }
    }
}
