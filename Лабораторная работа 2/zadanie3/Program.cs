﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie3
{
    class Program
    {
        static void Main(string[] args)
        {
            int x, y, z;
            double f;
            x = Convert.ToInt32(Console.ReadLine());
            y = Convert.ToInt32(Console.ReadLine());
            z = Convert.ToInt32(Console.ReadLine());
            f = 1 / 17.0 + Math.Log(Math.Abs(y)) / 7.0 + z / 7.0 + Math.Max(x, 1.0);
            Console.WriteLine(f);
            Console.ReadKey();
        }
    }
}
