﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie2
{
    class Program
    {
        static void Main(string[] args)
        {
            int x;
            double f;
            x = Convert.ToInt32(Console.ReadLine());
            f = 1 / 17.0 + Math.Log(Math.Abs(x)) / 7.0 + x / 7.0 + Math.Max(x, 1) + 2 * Math.Cos(x / 3.0) + Math.Pow(x, 1 / 4);
            Console.WriteLine(f);
            Console.ReadKey();
        }
    }
}
